import 'package:finda_flutter_application5/dashboard.dart';
import 'package:flutter/material.dart';



class plumberList extends StatelessWidget {
  const plumberList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Plumber List"),
        centerTitle: true,
      ),
      body: ElevatedButton(
        onPressed: () => {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => (const dashboard()),
              ))
        },
        child: const Text("Back To Dashboard"),
      ),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(height: 50.0),
      ),
    );
  }
}
