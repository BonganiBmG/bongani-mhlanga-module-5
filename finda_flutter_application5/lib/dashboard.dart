import 'package:finda_flutter_application5/profileedit.dart';

import 'package:flutter/material.dart';


import 'package:finda_flutter_application5/plumberList.dart';

class dashboard extends StatelessWidget {
  const dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("findaa Options"),
      ),
      body: Column(
        children: <Widget>[
          const Spacer(),
          const PlumberCard(),
          const Spacer(),
          ElevatedButton(
              onPressed: () => {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const plumberList()),
                    )
                  },
              child: const Text("Proceed To Plumbers"))
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(height: 50.0),
      ),
      floatingActionButton: FloatingActionButton(
          onPressed: () => {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const profileedit(),
                    ))
              },
          child: const Icon(Icons.admin_panel_settings)),
    );
  }
}

class PlumberCard extends StatelessWidget {
  const PlumberCard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Card(
        child: SizedBox(
          width: 300,
          height: 100,
          child: Center(child: Text('finda a plumber')),
        ),
      ),
    );
  }
}
