import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';


class profileedit extends StatelessWidget {
  const profileedit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    TextEditingController natureController = TextEditingController();
    TextEditingController locationController = TextEditingController();

    Future _addSessions() {
      final nature = natureController.text;
      final location = locationController.text;

      final ref = FirebaseFirestore.instance.collection("jobcards").doc();

      return ref
          .set({"Nature_Query": nature, "location": location})
          .then((value) => log("Job Card Created"))
          .catchError((onError) => log(onError));
    }

    return Scaffold(
      appBar: AppBar(
        title: const Text("Create Job Card"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: natureController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                  hintText: "Nature of Query i.e 'Blocked Drain'"),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: locationController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                  hintText: "Location of Query i.e 'Kitchen Area' "),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
            child: TextField(
              controller: locationController,
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)),
                  hintText: "Created By "),
            ),
          ),
          ElevatedButton(
              onPressed: (() {
                _addSessions();
              }),
              child: const Text("Generate Job Card"))
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        shape: const CircularNotchedRectangle(),
        child: Container(height: 50.0),
      ),
    );
  }
}
