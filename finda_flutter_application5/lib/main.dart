import 'dart:async';

import 'package:finda_flutter_application5/login.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  //await Firebase.initializeApp(
  //options: FirebaseOptions(
  //apiKey: "AIzaSyD3WKhvSHt9-4EOn34TfSqnYzbpdtIBZBY",
  //authDomain: "flutter-project1-98868.firebaseapp.com",
  //projectId: "flutter-project1-98868",
  //storageBucket: "flutter-project1-98868.appspot.com",
  //messagingSenderId: "826321717584",
  //appId: "1:826321717584:web:4b25b79ac42af3debfc455",
  //measurementId: "G-JV4P9E5RTX"));

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'finda.. App',
      home: FutureBuilder(
          future: _initialization,
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              print("Error");
            }
            if (snapshot.connectionState == ConnectionState.done) {
              return const Login();
            }
            return const CircularProgressIndicator();
          }),
      theme: ThemeData(
          //colorScheme: ColorScheme.fromSwatch(
          primarySwatch: Colors.deepPurple,
          //).copyWith(
          //secondary: Colors.lightBlueAccent,
          accentColor: Colors.purpleAccent,
          scaffoldBackgroundColor: Colors.lightBlue),

      //),

      //),
    );
  }
}
